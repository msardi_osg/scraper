FROM node:17-alpine3.12

# RUN apt-get update && apt-get install -y \
#     python3 python3-pip \
#     fonts-liberation libappindicator3-1 libasound2 libatk-bridge2.0-0 \
#     libnspr4 libnss3 lsb-release xdg-utils libxss1 libdbus-glib-1-2 \
#     curl unzip wget \
#     xvfb
RUN apk update
RUN apk upgrade
RUN apk add curl
# install geckodriver and firefox

RUN GECKODRIVER_VERSION='0.30' && \
    wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz && \
    tar -zxf geckodriver-v0.30.0-linux64.tar.gz -C /usr/local/bin && \
    chmod +x /usr/local/bin/geckodriver && \
    rm geckodriver-v0.30.0-linux64.tar.gz

# RUN FIREFOX_SETUP=firefox-setup.tar.bz2 && \
#     apt-get purge firefox && \
#     wget -O $FIREFOX_SETUP "https://download.mozilla.org/?product=firefox-latest&os=linux64" && \
#     tar xjf $FIREFOX_SETUP -C /opt/ && \
#     ln -s /opt/firefox/firefox /usr/bin/firefox && \
#     rm $FIREFOX_SETUP

RUN apk add firefox

ENV APP_HOME /usr/src/app
WORKDIR /$APP_HOME
COPY . $APP_HOME/

RUN npm install


CMD npm run start

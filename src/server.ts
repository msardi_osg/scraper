import { By, until, WebDriver, WebElement } from "selenium-webdriver";
import Dictionary from "./utils"
// import DbManager from './db'
import cliProgress from "cli-progress"

import webdriver from 'selenium-webdriver';
const firefox = require('selenium-webdriver/firefox');
require('dotenv').config()
require('events').EventEmitter.defaultMaxListeners = 40

const cq = require('concurrent-queue')

// const driver: WebDriver = new webdriver.Builder()
//     .forBrowser('firefox')
//     .setFirefoxOptions(new firefox.Options().headless())
//     .build();
// const dbClient = new DbManager()

async function loadHome(driver: WebDriver) {
    driver.get(String(process.env.BASE_URL));
}


// async function getProvincies(driver: WebDriver): Promise<[string, string][]> {
//     await driver.wait(until.elementLocated((By.id('ID0EDADAA'))));
//     let selectorProvincies = await driver.findElement(webdriver.By.id('ID0EDADAA'))
//     let valorsProvincies = await selectorProvincies.findElements(webdriver.By.tagName('option'));
//     const llistaProvincies: Array<[string, string]> = [];
//     for (const element of valorsProvincies) {
//         let idProvincia = await element.getAttribute('value')
//         let nomProvincia = await element.getText()
//         llistaProvincies.push([idProvincia, nomProvincia])
//     };
//     llistaProvincies.shift()
//     return llistaProvincies
// }
// 
// 
 async function closeCookiesBanner(driver: webdriver.WebDriver) {
     // Si no estem a la pagina inicial, hi tornem
     if (driver.getCurrentUrl().toString() != process.env.BASE_URL) {
         await loadHome(driver)
     }
     let closeCookiesButton = await driver.wait(until.elementLocated({ id : 'onetrust-reject-all-handler'}), 40 * 1000)
     closeCookiesButton.click()
 }
// async function loadProvinciaResults(provincia: [string, string], driver: webdriver.WebDriver) {
//     // Si no estem a la pagina inicial, hi tornem
//     if (driver.getCurrentUrl().toString() != process.env.BASE_URL) {
//         await loadHome(driver)
//     }
// 
//     // Seleccionem opció del dropdown de provincies i esperem redirect
//     await driver.findElement({ xpath: "//select[@id='ID0EDADAA']/option[@value=" + provincia[0] + "]" }).click()
//     await driver.findElement({ id: 'bt_matr1' }).click()
// }
// 
// 
// async function calcTotalPages(driver: WebDriver, provincia: string): Promise<number> {
//     if (driver.getCurrentUrl().toString() != process.env.SEARCH_RES_URL) {
//         await driver.get(String(process.env.SEARCH_RES_URL))
//     }
//     // Extraiem el nombre total de resultats
//     try {
//         let textResultsElement
//         try {
//             textResultsElement = await driver.wait(until.elementLocated({ xpath: "//*[contains(text(),'encontrados')]" }), 15 * 1000)
//         } catch (error) {
//             textResultsElement = await driver.wait(until.elementLocated({ xpath: "//*[contains(text(),'encontrados')]" }), 120 * 1000)
//         }
// 
// 
//         const textResults = await textResultsElement.getText()
//         var regex = /\d+/g;
//         var matches = textResults.match(regex);
//         // Càlcul del nombre de pagines
//         if (matches) {
//             // Dividim el nombre d'elements entre el nombre d'elements per pagina
//             let numPages = parseInt(matches[2]) / parseInt(String(process.env.RESULTS_PER_PAGE))
//             // Trunquem i sumem 1
//             numPages = Math.trunc(numPages) + 1
//             console.log(provincia + ' - num elements: ' + matches[2])
//             return numPages
//         } else {
//             return -1
//         }
//     } catch (err) {
//         console.log(provincia + ' stuck: num elements no trobat. Reintentant.')
//         return calcTotalPages(driver, provincia)
//     }
// }
// 
// 
// async function crawlPages(driver: WebDriver, provincia: string) {
//     const numPages = await calcTotalPages(driver, provincia)
//     // const bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
//     // bar.start(numPages - 1, 0)
//     let i = 1
//     try {
//         for (i; i <= numPages; i++) {
//             await driver.get(String(process.env.RESULTS_URL) + String(i))
//             await scrapPage(driver, provincia)
//         }
//     } catch (error) {
//         console.log("Retrying " + provincia + " from page " + String(i))
//         await driver.get(String(process.env.RESULTS_URL) + String(i))
//         await scrapPage(driver, provincia)
//     }
// 
// }
// 
// 
// 
// async function scrapPage(driver: WebDriver, provincia: string) {
//     const tuples = []
// 
//     let resTables = await driver.findElements({ id: 'tabla_cn' })
//     let resTable = resTables[1]
//     try {
//         if (!resTable) {
//             await driver.wait(until.elementLocated({ id: 'tabla_cn' }), 15 * 1000)
//             resTables = await driver.findElements({ id: 'tabla_cn' })
//             resTable = resTables[1]
//         }
//     } catch (error) {
//         console.log('Warning: ' + provincia + ' is stale. Last chance (120s)')
//         await driver.wait(until.elementLocated({ id: 'tabla_cn' }), 120 * 1000)
//         resTables = await driver.findElements({ id: 'tabla_cn' })
//         console.log(resTables.length)
//         resTable = resTables[1]
//     }
// 
// 
//     const resTableRows = await resTable.findElements({ css: 'tr' })
//     for (const tableRow of resTableRows) {
//         const rowCells = await tableRow.findElements({ css: 'td' })
//         if (rowCells.length > 0) {
//             const tuple = {
//                 "codiIdentificacio": await rowCells[0].getText(),
//                 "nomDenomSocial": await rowCells[1].getText(),
//                 "dadesContacte": await rowCells[2].getText(),
//                 "categoria": await rowCells[3].getText(),
//                 "nivell": await rowCells[4].getText(),
//                 "caducitatInsc": await rowCells[5].getText()
//             }
//             tuples.push(tuple)
//         }
//     }
//     dbClient.saveTuples(provincia, tuples)
// 
// }



async function main() {
    const driver: WebDriver = new webdriver.Builder()
        .forBrowser('firefox')
//        .setFirefoxOptions(new firefox.Options().headless())
        .build();
    await loadHome(driver);
    await closeCookiesBanner(driver);



//    const llistaProvincies = await getProvincies(driver)
//    driver.quit()
//    const queue = cq().limit({ concurrency: 20 }).process(function (provincia: [string, string]) {
//        return new Promise(function (resolve, reject) {
//            const localDriver = new webdriver.Builder()
//                .forBrowser('firefox')
//                // .setFirefoxOptions(new firefox.Options().headless())
//                .build()
//            try {
//                loadProvinciaResults(provincia, localDriver).then(() => {
//                    crawlPages(localDriver, provincia[1]).then(() => {
//                        localDriver.quit()
//                    })
//                })
//            } catch (error) {
//                console.log(error)
//                localDriver.quit()
//            }
//
//
//        })
//    })
//    const delay = (ms: number | undefined) => new Promise(resolve => setTimeout(resolve, ms))
//    for (const provincies of llistaProvincies) {
//        await delay(1000)
//        queue(provincies).then(function (provincia: [string, string]) {
//            console.log(provincies + ' llest!')
//        })
//    }


}


main()

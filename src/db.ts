import { MongoClient } from 'mongodb';
import dayjs from 'dayjs';

class DbManager {
    uri: string
    client: MongoClient
    dbName: string;

    constructor() {
        this.dbName = 'ROPO_' + dayjs(new Date(), "yyyymmdd_hhMMss");
        this.uri = 'mongodb://localhost:27017/' + this.dbName
        this.client = new MongoClient(this.uri)
        this.client.connect()

    }

    async close() {
        await this.client.close();
    }

    async listDatabases() {
        const databasesList = await this.client.db().admin().listDatabases();

        console.log("Databases:");
        databasesList.databases.forEach((db: { name: any; }) => console.log(` - ${db.name}`));
    }

    saveTuples(collection: string, tuples: any[]) {
        if (tuples.length > 0) {
            var col = this.client.db(this.dbName).collection(collection).insertMany(tuples,
                function (err, res) {
                    if (err) throw err;
                })
        }
    }
}

export default DbManager